/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
} from 'react-native';

import HomeScreen from "./src/screens/HomeScreen";
import AnimationScreen from "./src/screens/AnimationScreen";

const App: () => React$Node = () => {
    return (<SafeAreaView style={styles.container}>
        {/*<HomeScreen/>*/}
        <AnimationScreen/>
    </SafeAreaView>);
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3e3e3e',
    }
});

export default App;
