import React, {useState} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    SafeAreaView,
    UIManager,
    LayoutAnimation
} from 'react-native';

if (
    Platform.OS === 'android' &&
    UIManager.setLayoutAnimationEnabledExperimental
) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

const SmoothAnimationScreen = props =>{
    const [expanded, setExpanded ] = useState(false);
    return (<SafeAreaView>
        <View style={{overflow: 'hidden'}}>
            <TouchableOpacity
                onPress={() => {
                    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                    setExpanded(!expanded);
                }}>
                <Text>
                    Press me to {expanded ? 'collapse' : 'expand'}!
                </Text>
            </TouchableOpacity>
            {expanded && <Text>I disappear sometimes!</Text>}
        </View></SafeAreaView>);
};

export default SmoothAnimationScreen

