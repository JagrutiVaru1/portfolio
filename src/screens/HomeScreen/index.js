import React, {useState, useEffect} from 'react';
import {
    View,
    Text,
    PanResponder,
    Animated,
    ImageBackground,
} from 'react-native';
import styles from './styles';

const HomeScreen =  props => {

    const [pan, setPan] = useState(new Animated.ValueXY());
    const [panResponder, setPanResponder] = useState(new Animated.ValueXY());

    useEffect(() => {
        this._val = { x:0, y:0 };
        pan.addListener((value) => this._val = value);

        setPanResponder(PanResponder.create({
            onStartShouldSetPanResponder: (e, gesture) => true,
            onPanResponderGrant: (e, gesture) => {
                pan.setOffset({
                    x: this._val.x,
                    y:this._val.y
                });
                pan.setValue({ x:0, y:0})
            },
            onPanResponderMove: Animated.event([
                null, { dx: pan.x, dy: pan.y }
            ]),
            onPanResponderRelease: (e, gesture) => {
            }
        }));
    }, [pan, setPanResponder]);


    const panStyle = {
        transform: pan.getTranslateTransform()
    };

    return (<View style={styles.container}>
        <ImageBackground
            source={{uri: 'https://placeimg.com/640/480/people'}}
            style={styles.imageBackground}>
            <View style={styles.myname}>
                <Text style={styles.myNameTextStyles}>VARU</Text>
                <Text style={styles.myNameTextStyles}>JAGRUTI</Text>
            </View>
            <View style={styles.footerContainer}>
                <Animated.View style={[styles.whoAmIContainer,panStyle]}
                               {...panResponder.panHandlers}>
                    <Text style={styles.whoAmIHeader}> WHO AM I</Text>
                </Animated.View>
                <View style={styles.skillContainer}>
                    <View style={styles.skillInnerContainer}>
                        <Text style={styles.skillHeader}> MY SKILLS</Text>
                    </View>
                </View>
            </View>
        </ImageBackground>
    </View>);
};

export default HomeScreen;
