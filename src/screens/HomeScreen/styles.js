import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#474b5c',
        flex: 1,
        flexDirection: 'column'
    },
    imageBackground: {
        resizeMode: 'center',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        height: Dimensions.get('window').height,
        backgroundColor: 'gray'
    },
    myname: {
        flex: 5,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        paddingBottom: '7%',
        paddingLeft: '20%',
    },
    myNameTextStyles: {
        fontSize: 35,
        fontWeight: 'bold',
        color: 'white'
    },
    footerContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: '100%'
    },
    whoAmIContainer: {
        backgroundColor: 'black',
        height: 85,
        borderTopLeftRadius: 35,
        width: '100%',
        paddingLeft: '20%',
        paddingTop: '5%'
    },
    whoAmIHeader: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white'
    },
    skillContainer: {
        backgroundColor: 'black',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'flex-end'
    },
    skillInnerContainer: {
        backgroundColor: 'white',
        height: 85,
        borderTopLeftRadius: 35,
        width: '85%',
        paddingLeft: '5%',
        paddingTop: '5%'
    },
    skillHeader: {
        fontSize: 30,
        fontWeight: 'bold'
    }
})
