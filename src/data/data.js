export default [
    {
        title: 'nature',
        jobId:'1',
        color: 'red',
        imageUrl:'https://placeimg.com/640/480/nature'
    },
    {
        title: 'animals',
        jobId:'2',
        color: 'blue',
        imageUrl:'https://placeimg.com/640/480/animals'
    },
    {
        title: 'architects',
        jobId:'3',
        color: 'green',
        imageUrl:'https://placeimg.com/640/480/arch'
    },
    {
        title: 'people',
        jobId:'4',
        color: 'gray',
        imageUrl:'https://placeimg.com/640/480/people'
    },
    {
        title: 'technology',
        jobId:'5',
        color: 'red',
        imageUrl:'https://placeimg.com/640/480/tech'
    },
    {
        title: 'sepia',
        jobId:'6',
        color: 'blue',
        imageUrl:'https://placeimg.com/640/480/tech/sepia'
    },
    {
        title: 'animals',
        jobId:'7',
        color: 'green',
        imageUrl:'https://placeimg.com/640/480/animals'
    },
    {
        title: 'technology',
        jobId:'8',
        color: 'gray',
        imageUrl:'https://placeimg.com/640/480/arch'
    },
    // another jobs
]
